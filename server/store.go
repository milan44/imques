package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"time"
)

func loadStore() error {
	if _, err := os.Stat(loadedConfig.Store); os.IsNotExist(err) {
		queue = make([]string, 0)

		incrVersion()
		return nil
	}

	dat, err := ioutil.ReadFile(loadedConfig.Store)
	if err != nil {
		return err
	}

	err = json.Unmarshal(dat, &queue)
	if err != nil {
		queue = make([]string, 0)
		return err
	}

	return nil
}

var storeHasChanged = false

func incrVersion() {
	storeHasChanged = true
}

func persistStore() {
	queueMutex.Lock()
	dat, _ := json.Marshal(queue)
	queueMutex.Unlock()

	err := ioutil.WriteFile(loadedConfig.Store, dat, 0777)

	if err != nil {
		log.Println("Failed to persist queue: " + err.Error())
	}
}

func persistingLoop() {
	go func() {
		for {
			if storeHasChanged {
				storeHasChanged = false
				persistStore()
			}

			time.Sleep(10 * time.Second)
		}
	}()
}
