package main

import (
	"bufio"
	"encoding/json"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

func encode(s string) string {
	d, _ := json.Marshal(s)
	return string(d)
}
func decode(s string) string {
	var d string
	_ = json.Unmarshal([]byte(s), &d)
	return d
}
func addr(conn net.Conn) string {
	remoteAddr := conn.RemoteAddr().String()
	remoteAddr = strings.Replace(remoteAddr, "[::1]", "localhost", 1)
	return remoteAddr
}

var connectionCount = 0
var connectionCountMutex = sync.Mutex{}

func handleConnection(conn net.Conn) {
	connectionCountMutex.Lock()
	connectionCount++
	connectionCountMutex.Unlock()

	log.Println(addr(conn) + " connected (" + strconv.Itoa(connectionCount) + " connections total)")

	scanner := bufio.NewScanner(conn)

	forceCloseCounter := 0

	for {
		ch := make(chan string, 1)

		go func() {
			ok := scanner.Scan()

			resp := decode(scanner.Text())

			if !ok {
				resp = ""
				forceCloseCounter++
			} else {
				forceCloseCounter = 0
			}

			ch <- resp
		}()

		resp := ""
		timedOut := false

		select {
		case resp = <-ch:
			if resp == "" {
				if forceCloseCounter > 10 {
					log.Println(addr(conn) + " appears to be closed")
					timedOut = true
				} else {
					continue
				}
			}
		case <-time.After(1 * time.Minute):
			log.Println(addr(conn) + " timed out")
			timedOut = true
		}

		if timedOut || handleMessage(resp, conn) {
			break
		}
	}

	_ = conn.Close()

	connectionCountMutex.Lock()
	connectionCount--
	connectionCountMutex.Unlock()

	log.Println(addr(conn) + " disconnected (" + strconv.Itoa(connectionCount) + " connections total)")
}
