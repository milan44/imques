package main

import (
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
)

var loadedConfig Configugation

func main() {
	log.SetFlags(0)
	log.SetOutput(new(logWriter))

	args := os.Args[1:]
	configFile := defaultConfigurationFile

	if len(args) > 0 {
		configFile = strings.Join(args, " ")
	}

	lcfg, err := readConfig(configFile)
	if err != nil {
		die("Failed to load config: " + err.Error())
	}
	loadedConfig = lcfg

	err = loadStore()
	if err != nil {
		die("Failed to load stored queue: " + err.Error())
	}

	queueMutex = sync.Mutex{}

	persistingLoop()

	listener, _ := net.Listen("tcp", ":"+strconv.Itoa(loadedConfig.Port))

	defer listener.Close()

	log.Println("Waiting for connections")

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Some connection error: %s\n", err)
			_ = conn.Close()
			continue
		}

		go handleConnection(conn)
	}
}

func die(err string) {
	log.Println(err)
	os.Exit(1)
}
