package main

import (
	"net"
	"strconv"
	"strings"
	"sync"
)

const (
	okResponse    = "OK"
	errorResponse = "FAILED"
	emptyResponse = "NIL"
)

var queue []string
var queueMutex sync.Mutex

func handleMessage(message string, conn net.Conn) bool {
	command := strings.Split(strings.TrimSpace(message), " ")[0]
	tcommand := strings.ToUpper(command)

	args := strings.TrimSpace(strings.Replace(message, command, "", 1))

	if tcommand == "PUSH" {
		queueMutex.Lock()
		queue = append(queue, args)
		queueMutex.Unlock()

		respond(okResponse, conn)

		incrVersion()
	} else if tcommand == "POP" {
		queueMutex.Lock()
		if len(queue) == 0 {
			respond(emptyResponse, conn)
		} else {
			data := queue[0]
			queue = queue[1:]
			respond(okResponse+": "+data, conn)
		}
		queueMutex.Unlock()

		incrVersion()
	} else if tcommand == "SHOW" {
		queueMutex.Lock()
		data := queue
		queueMutex.Unlock()

		if len(data) == 0 {
			respond(emptyResponse, conn)
			return false
		}

		if args != "" {
			l, err := strconv.Atoi(args)
			if err != nil {
				respond(errorResponse, conn)
				return false
			}

			data = data[:l]
		}

		respond(okResponse+": "+strings.Join(data, ", "), conn)
	} else if tcommand == "SIZE" {
		queueMutex.Lock()
		respond(okResponse+": "+strconv.Itoa(len(queue)), conn)
		queueMutex.Unlock()
	} else if tcommand == "BYE" {
		respond("BYE", conn)
		conn.Close()
		return true
	} else if tcommand == "PING" {
		respond("PONG", conn)
	} else {
		respond(errorResponse, conn)
	}
	return false
}

func respond(message string, conn net.Conn) {
	message = encode(message)
	conn.Write([]byte(message + "\n"))
}
