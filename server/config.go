package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type Configugation struct {
	Port  int    `yaml:"port"`
	Store string `yaml:"store"`
}

var DefaultConfiguration = Configugation{
	Port:  8998,
	Store: "./imques.store",
}

const defaultConfigurationFile = "./imques-config.yaml"

func readConfig(config string) (Configugation, error) {
	if _, err := os.Stat(config); os.IsNotExist(err) {
		writeDefaultConfig()
		return DefaultConfiguration, nil
	}

	configuration := DefaultConfiguration

	bcfg, err := ioutil.ReadFile(config)
	if err != nil {
		return configuration, err
	}

	err = yaml.Unmarshal(bcfg, &configuration)
	if err != nil {
		return configuration, err
	}

	return configuration, nil
}

func writeDefaultConfig() {
	cfg, err := yaml.Marshal(DefaultConfiguration)
	if err != nil {
		return
	}

	_ = ioutil.WriteFile(defaultConfigurationFile, cfg, 0777)
}
