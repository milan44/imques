package imques

import (
	"bufio"
	"encoding/json"
	"errors"
	"net"
	"strconv"
	"strings"
	"time"
)

type ImquesConnection struct {
	HostString string
	active     bool
	Connection net.Conn
	Scanner    *bufio.Scanner
}

func encode(s string) string {
	d, _ := json.Marshal(s)
	return string(d)
}
func decode(s string) string {
	var d string
	_ = json.Unmarshal([]byte(s), &d)
	return d
}

func NewImquesConnection(host string, port int) (ImquesConnection, error) {
	conn := ImquesConnection{
		HostString: host + ":" + strconv.Itoa(port),
		active:     false,
	}

	connection, err := net.Dial("tcp", conn.HostString)
	if err != nil {
		if strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it") {
			return conn, errors.New("remote machine is online but imques server is not running")
		}
		return conn, err
	}
	conn.Connection = connection

	sc := bufio.NewScanner(connection)
	if sc == nil {
		return conn, errors.New("failed to create scanner")
	}
	conn.Scanner = sc

	conn.active = true
	ping := conn.Ping()
	if ping != nil {
		conn.active = false
		return conn, ping
	}

	conn.active = true
	conn.keepAliveLoop()

	return conn, nil
}

func (i *ImquesConnection) keepAliveLoop() {
	go func(i *ImquesConnection) {
		for i.active {
			time.Sleep(45 * time.Second)
			_ = i.Ping()
		}
	}(i)
}

func (i *ImquesConnection) write(data string) error {
	if !i.active {
		return errors.New("no connection to server present")
	}

	data = encode(data) + "\n"

	_, err := i.Connection.Write([]byte(data))
	return err
}
func (i *ImquesConnection) read() (string, error) {
	ch := make(chan string, 1)

	go func() {
		ok := i.Scanner.Scan()
		resp := decode(i.Scanner.Text())

		if !ok {
			resp = ""
		}

		ch <- resp
	}()

	select {
	case res := <-ch:
		if res == "" {
			i.active = false
			return "", errors.New("disconnected from server")
		}
		return res, nil
	case <-time.After(5 * time.Second):
		return "", errors.New("server timed out")
	}
}

func (i *ImquesConnection) Send(data string) (string, error) {
	err := i.write(data)
	if err != nil {
		if strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") {
			i.active = false
			return "", errors.New("disconnected from server")
		} else if strings.Contains(err.Error(), "An established connection was aborted by the software in your host machine") {
			i.active = false
			return "", errors.New("server closed the connection")
		}
		return "", err
	}

	return i.read()
}

func (i *ImquesConnection) Ping() error {
	response, err := i.Send("PING")
	if err != nil {
		return err
	} else if response != "PONG" {
		return errors.New("response was not pong (was " + response + ")")
	}
	return nil
}
func (i *ImquesConnection) Push(data string) error {
	response, err := i.Send("PUSH " + data)
	if err != nil {
		return err
	} else if response != "OK" {
		return errors.New("response was not ok (was " + response + ")")
	}
	return nil
}
func (i *ImquesConnection) Pop() (string, error) {
	response, err := i.Send("POP")
	if err != nil {
		return "", err
	} else if response == "NIL" {
		return "", nil
	} else if !strings.HasPrefix(response, "OK: ") {
		return "", errors.New("response was not ok (was " + response + ")")
	}

	return strings.Replace(response, "OK: ", "", 1), nil
}
func (i *ImquesConnection) Size() (int, error) {
	response, err := i.Send("SIZE")
	if err != nil {
		return 0, err
	} else if !strings.HasPrefix(response, "OK: ") {
		return 0, errors.New("response was not ok (was " + response + ")")
	}

	in, err := strconv.Atoi(strings.Replace(response, "OK: ", "", 1))
	if err != nil {
		return 0, err
	}

	return in, nil
}
func (i *ImquesConnection) Disconnect() error {
	response, err := i.Send("BYE")
	if err != nil {
		return err
	} else if response != "BYE" {
		return errors.New("response was not bye (was " + response + ")")
	}

	i.active = false
	_ = i.Connection.Close()
	return nil
}
