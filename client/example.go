package main

import (
	"bufio"
	"fmt"
	"gitlab.com/milan44/imques/sdk"
	"os"
	"strconv"
)

func main() {
	host := "localhost"
	port := 8998

	if len(os.Args) > 1 {
		host = os.Args[1]
	}
	if len(os.Args) > 2 {
		in, err := strconv.Atoi(os.Args[2])
		if err != nil {
			fmt.Println("Failed to parse '" + os.Args[2] + "' to int")
		} else {
			port = in
		}
	}

	fmt.Println("Connecting to " + host + ":" + strconv.Itoa(port) + "...")

	conn, err := imques.NewImquesConnection("localhost", 8998)
	if err != nil {
		fmt.Println("Error: " + err.Error())
		return
	}
	fmt.Println("Connected!")

	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("> ")
		text, _ := reader.ReadString('\n')

		resp, err := conn.Send(text)
		if err != nil {
			fmt.Println("Error: " + err.Error())
		} else {
			fmt.Println(resp)
			if resp == "BYE" {
				return
			}
		}
	}
}
