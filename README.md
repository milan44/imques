# Imques

Imques is an in memory queue database. This means it keeps track of a queue. You can push to the queue and pop from it. This is especially useful if you have multiple clients working on a queue and you don't want multiple clients working on the same entry in the queue. Imques will make sure the queue is concurrency safe.

## Usage

### Server

The server just needs to be built (`go build`) and then run (remember to install the dependencies by running the `dependencies.sh`). It takes the path to your config as its first argument. If you don't have a config it will automatically generate one in your current working directory. You can configure your Imques server by modifying the config yaml file.

### Client

The CLI client (similar to the server) only needs to be built (`go build`) and then run (remember to install the dependencies by running the `dependencies.sh`). It takes the hostname as its first and the port as its second argument.

### SDK

The SDK is pretty self explanatory. You get an `ImquesConnection` by using the `NewImquesConnection(host string, port int)` function. It will return an error if it can't connect to the server. You can then run any command that you would run in the CLI client by using the respective function on your `ImquesConnection` struct.

## Commands

### PUSH

PUSH will push a string to the end of the queue. The string can have any content (multiline, etc.).  
**Example:**
```
PUSH Hello World
```

### POP

POP will remove the first element from the queue and return it to you. If the queue is empty it will return `NIL` (SDK will just return an empty string).

### SHOW

SHOW will how you the entire queue. You can specify how many entries it should show max. If the queue is empty it will return `NIL`.  
**Example:** (Will show the first 12 entries)
```
SHOW 12
```

### SIZE

SIZE will return the amount of entries in the queue.

### BYE

BYE disconnects you from the server.